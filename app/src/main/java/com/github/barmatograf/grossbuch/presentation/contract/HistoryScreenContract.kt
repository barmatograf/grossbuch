package com.github.barmatograf.grossbuch.presentation.contract

import com.github.barmatograf.grossbuch.model.entity.Filter
import com.github.barmatograf.grossbuch.model.entity.Transaction

interface HistoryScreenContract : BaseContract {

    interface Router : BaseContract.Router {

        fun startFiltersScreenForUpdate(filter: Filter): Result<Filter>
        fun closeScreen()
    }

    interface View : BaseContract.View {

        fun showTransactions(transactions: List<Transaction>)
    }

    interface Presenter : BaseContract.Presenter<Router, View> {

        fun onTransactionsSearchByFilter(filter: Filter)
        fun onFilterEdit()
        fun onCancel()
    }
}