package com.github.barmatograf.grossbuch.model.mock

import com.github.barmatograf.grossbuch.model.entity.Category
import com.github.barmatograf.grossbuch.model.mock.exception.CategoryNotFoundException
import com.github.barmatograf.grossbuch.model.repository.CategoriesRepository
import java.util.function.Predicate

class CategoriesRepositoryMock : CategoriesRepository {
    private val data = mutableMapOf<Int, Category>()
    private var identifierCounter = 0

    override suspend fun add(entity: Category): Result<Int> {
        val identifier = ++identifierCounter
        val category = Category(
            identifier,
            entity.title, entity.type, entity.iconUri
        )
        data[identifier] = category
        return Result.success(identifier)
    }

    override suspend fun getByIdentifier(identifier: Int): Result<Category> {
        return if (data.containsKey(identifier)) {
            Result.success(data.getValue(identifier))
        } else {
            Result.failure(CategoryNotFoundException(identifier))
        }
    }

    override suspend fun getAll(): Result<List<Category>> {
        return Result.success(data.values.toList())
    }

    override suspend fun getAllByFilters(
        vararg filters: Predicate<Category>
    ): Result<List<Category>> {
        val categories = data.values.filter { category ->
            filters.all { filter ->
                filter.test(category)
            }
        }
        return Result.success(categories)
    }

    override suspend fun contains(entity: Category): Result<Boolean> {
        return Result.success(data.containsKey(entity.identifier))
    }

    override suspend fun update(entity: Category): Result<Unit> {
        return if (data.containsKey(entity.identifier)) {
            data[entity.identifier] = entity
            Result.success(Unit)
        } else {
            Result.failure(CategoryNotFoundException(entity.identifier))
        }
    }

    override suspend fun remove(entity: Category): Result<Unit> {
        return if (data.containsKey(entity.identifier)) {
            data.remove(entity.identifier)
            Result.success(Unit)
        } else {
            Result.failure(CategoryNotFoundException(entity.identifier))
        }
    }
}