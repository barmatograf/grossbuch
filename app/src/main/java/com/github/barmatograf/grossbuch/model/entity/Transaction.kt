package com.github.barmatograf.grossbuch.model.entity

import java.time.LocalDate

data class Transaction(
    val identifier: Int,
    val amount: Int,
    val date: LocalDate,
    val category: Category,
    val description: String?,
    val photoUri: String?
)