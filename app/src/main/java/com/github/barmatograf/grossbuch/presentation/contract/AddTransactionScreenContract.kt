package com.github.barmatograf.grossbuch.presentation.contract

import com.github.barmatograf.grossbuch.model.entity.Category
import com.github.barmatograf.grossbuch.model.entity.Description
import com.github.barmatograf.grossbuch.model.entity.Transaction

interface AddTransactionScreenContract : BaseContract {

    interface Router : BaseContract.Router {

        fun startAddDescriptionScreenForResult(): Result<Description>
        fun closeScreen()
    }

    interface View : BaseContract.View {

        fun setCategoriesMenu(categories: List<Category>)
        fun setDescription(description: Description)
    }

    interface Presenter : BaseContract.Presenter<Router, View> {

        fun onCategoriesSearchByTitle(title: String)
        fun onDescriptionCreation()
        fun onTransactionAdd(transaction: Transaction)
        fun onCancel()
    }
}