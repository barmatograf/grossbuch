package com.github.barmatograf.grossbuch.presentation.contract

import com.github.barmatograf.grossbuch.model.entity.Category
import java.time.LocalDate

interface MainScreenContract : BaseContract {

    interface Router : BaseContract.Router {

        fun startAddTransactionScreen()
        fun startHistoryScreenWithCategory(category: Category)
        fun startHistoryScreenWithDate(date: LocalDate)
    }

    interface View : BaseContract.View {

        fun showCategoriesScreen()
        fun showStatisticsScreen()
    }

    interface Presenter : BaseContract.Presenter<Router, View> {

        fun onAddTransactionScreenView()
        fun onCategoriesScreenView()
        fun onStatisticsScreenView()
    }
}