package com.github.barmatograf.grossbuch.model.repository

import java.util.function.Predicate

interface Repository<E> {

    suspend fun add(entity: E): Result<Int>
    suspend fun getByIdentifier(identifier: Int): Result<E>
    suspend fun getAll(): Result<List<E>>
    suspend fun getAllByFilters(vararg filters: Predicate<E>): Result<List<E>>
    suspend fun contains(entity: E): Result<Boolean>
    suspend fun update(entity: E): Result<Unit>
    suspend fun remove(entity: E): Result<Unit>
}