package com.github.barmatograf.grossbuch.model.entity

data class Description(val text: String?, val photo: String?)