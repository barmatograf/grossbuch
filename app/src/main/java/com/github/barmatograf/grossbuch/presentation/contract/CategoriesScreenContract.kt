package com.github.barmatograf.grossbuch.presentation.contract

import com.github.barmatograf.grossbuch.model.entity.Category

interface CategoriesScreenContract : BaseContract {

    interface Router : BaseContract.Router {

        fun startHistoryScreenWithCategory(category: Category)
    }

    interface View : BaseContract.View {

        fun showCategories(categories: List<Category>)
    }

    interface Presenter : BaseContract.Presenter<Router, View> {

        fun onCategoriesView()
        fun onCategorySelection(category: Category)
    }
}