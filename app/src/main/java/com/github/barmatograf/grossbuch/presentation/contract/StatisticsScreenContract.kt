package com.github.barmatograf.grossbuch.presentation.contract

import com.github.barmatograf.grossbuch.model.entity.Transaction
import java.time.LocalDate

interface StatisticsScreenContract : BaseContract {

    interface Router : BaseContract.Router {

        fun startHistoryScreenWithDate(date: LocalDate)
    }

    interface View : BaseContract.View {

        fun showCharts(transactions: List<Transaction>)
    }

    interface Presenter : BaseContract.Presenter<Router, View> {

        fun onTransactionsViewInDatesInterval(from: LocalDate, to: LocalDate)
    }
}