package com.github.barmatograf.grossbuch.model.repository

import com.github.barmatograf.grossbuch.model.entity.Category

interface CategoriesRepository : Repository<Category>