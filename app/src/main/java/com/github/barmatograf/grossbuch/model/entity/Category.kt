package com.github.barmatograf.grossbuch.model.entity

data class Category(
    val identifier: Int,
    val title: String,
    val type: Type,
    val iconUri: String
)