package com.github.barmatograf.grossbuch.model.interactor

import com.github.barmatograf.grossbuch.model.entity.Category

fun interface FindCategoriesByTitleInteractor {

    fun invoke(title: String): Result<List<Category>>
}