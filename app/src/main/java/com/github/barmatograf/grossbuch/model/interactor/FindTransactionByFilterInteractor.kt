package com.github.barmatograf.grossbuch.model.interactor

import com.github.barmatograf.grossbuch.model.entity.Filter
import com.github.barmatograf.grossbuch.model.entity.Transaction

fun interface FindTransactionByFilterInteractor {

    fun invoke(filter: Filter): Result<List<Transaction>>
}