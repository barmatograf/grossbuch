package com.github.barmatograf.grossbuch.model.mock.exception

class TransactionNotFoundException(identifier: Int) :
    EntityNotFoundException("Transaction", identifier)