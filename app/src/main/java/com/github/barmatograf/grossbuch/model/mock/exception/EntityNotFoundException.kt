package com.github.barmatograf.grossbuch.model.mock.exception

open class EntityNotFoundException(name: String, identifier: Int) :
    RuntimeException("Entity '$name' with identifier '$identifier' not found!")