package com.github.barmatograf.grossbuch.presentation.contract

import com.github.barmatograf.grossbuch.model.entity.Description

interface AddDescriptionScreenContract : BaseContract {

    interface Router : BaseContract.Router {

        fun closeScreenWithResult(description: Description)
        fun closeScreen()
    }

    interface View : BaseContract.View

    interface Presenter : BaseContract.Presenter<Router, View> {

        fun onApply(description: Description)
        fun onCancel()
    }
}