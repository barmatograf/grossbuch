package com.github.barmatograf.grossbuch.model.entity

enum class Type {
    REVENUE,
    EXPENSE
}