package com.github.barmatograf.grossbuch.model.interactor

import com.github.barmatograf.grossbuch.model.entity.Category

fun interface GetAllCategoriesInteractor {

    fun invoke(): Result<List<Category>>
}