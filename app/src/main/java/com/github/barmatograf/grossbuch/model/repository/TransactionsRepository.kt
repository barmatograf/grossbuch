package com.github.barmatograf.grossbuch.model.repository

import com.github.barmatograf.grossbuch.model.entity.Transaction

interface TransactionsRepository : Repository<Transaction>