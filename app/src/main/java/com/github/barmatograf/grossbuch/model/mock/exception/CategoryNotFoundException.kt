package com.github.barmatograf.grossbuch.model.mock.exception

class CategoryNotFoundException(identifier: Int) : EntityNotFoundException("Category", identifier)