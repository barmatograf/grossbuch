package com.github.barmatograf.grossbuch.model.interactor

import com.github.barmatograf.grossbuch.model.entity.Category

fun interface RemoveCategoryInteractor {

    fun invoke(category: Category): Result<Unit>
}