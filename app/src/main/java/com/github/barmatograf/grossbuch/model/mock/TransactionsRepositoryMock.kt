package com.github.barmatograf.grossbuch.model.mock

import com.github.barmatograf.grossbuch.model.entity.Transaction
import com.github.barmatograf.grossbuch.model.mock.exception.CategoryNotFoundException
import com.github.barmatograf.grossbuch.model.mock.exception.TransactionNotFoundException
import com.github.barmatograf.grossbuch.model.repository.CategoriesRepository
import com.github.barmatograf.grossbuch.model.repository.TransactionsRepository
import java.util.function.Predicate

class TransactionsRepositoryMock(
    private val categories: CategoriesRepository
) : TransactionsRepository {
    private val data = mutableMapOf<Int, Transaction>()
    private var identifierCounter = 0

    override suspend fun add(entity: Transaction): Result<Int> {
        val available = categories
            .contains(entity.category)
            .getOrElse {
                return Result.failure(it)
            }
        return if (available) {
            val identifier = ++identifierCounter
            val transaction = Transaction(
                identifier,
                entity.amount, entity.date, entity.category, entity.description, entity.photoUri
            )
            data[identifier] = transaction
            Result.success(identifier)
        } else {
            Result.failure(CategoryNotFoundException(entity.category.identifier))
        }
    }

    override suspend fun getByIdentifier(identifier: Int): Result<Transaction> {
        return if (data.containsKey(identifier)) {
            Result.success(data.getValue(identifier))
        } else {
            Result.failure(TransactionNotFoundException(identifier))
        }
    }

    override suspend fun getAll(): Result<List<Transaction>> {
        return Result.success(data.values.toList())
    }

    override suspend fun getAllByFilters(
        vararg filters: Predicate<Transaction>
    ): Result<List<Transaction>> {
        val transactions = data.values.filter { transaction ->
            filters.all { filter ->
                filter.test(transaction)
            }
        }
        return Result.success(transactions)
    }

    override suspend fun contains(entity: Transaction): Result<Boolean> {
        return Result.success(data.containsKey(entity.identifier))
    }

    override suspend fun update(entity: Transaction): Result<Unit> {
        if (!data.containsKey(entity.identifier)) {
            return Result.failure(TransactionNotFoundException(entity.identifier))
        }
        val available = categories
            .contains(entity.category)
            .getOrElse {
                return Result.failure(it)
            }
        return if (available) {
            data[entity.identifier] = entity
            Result.success(Unit)
        } else {
            Result.failure(CategoryNotFoundException(entity.category.identifier))
        }
    }

    override suspend fun remove(entity: Transaction): Result<Unit> {
        return if (data.containsKey(entity.identifier)) {
            data.remove(entity.identifier)
            Result.success(Unit)
        } else {
            Result.failure(TransactionNotFoundException(entity.identifier))
        }
    }
}