package com.github.barmatograf.grossbuch.model.entity

import java.time.LocalDate

data class Filter(
    val types: Set<Type>,
    val categories: Set<Category>,
    val fromDate: LocalDate,
    val toDate: LocalDate
)