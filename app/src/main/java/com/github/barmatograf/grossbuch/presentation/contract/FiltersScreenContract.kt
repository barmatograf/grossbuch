package com.github.barmatograf.grossbuch.presentation.contract

import com.github.barmatograf.grossbuch.model.entity.Category
import com.github.barmatograf.grossbuch.model.entity.Filter

interface FiltersScreenContract : BaseContract {

    interface Router : BaseContract.Router {

        fun closeScreenWithResult(filter: Filter)
        fun closeScreen()
    }

    interface View : BaseContract.View {

        fun showFilteredCategories(categories: List<Category>)
    }

    interface Presenter : BaseContract.Presenter<Router, View> {

        fun onCategoriesSearchExcluding(title: String, categories: List<Category>)
        fun onApply(filter: Filter)
        fun onCancel()
    }
}