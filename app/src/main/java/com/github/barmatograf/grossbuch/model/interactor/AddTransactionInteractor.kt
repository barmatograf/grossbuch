package com.github.barmatograf.grossbuch.model.interactor

import com.github.barmatograf.grossbuch.model.entity.Transaction

fun interface AddTransactionInteractor {

    fun invoke(transaction: Transaction): Result<Int>
}