package com.github.barmatograf.grossbuch.presentation.contract

interface BaseContract {

    interface Router

    interface View {

        fun showError(description: String)
    }

    interface Presenter<R : Router, V : View> {

        fun attachView(view: V)
        fun detachView()
        fun attachRouter(router: R)
        fun detachRouter()
    }
}